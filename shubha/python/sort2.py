import random

num = int(input("Enter the number of terms in the list: "))
myList=[]
for i in range (1, num+1):
    myList.append(random.randint(1,101))
	
print("List before sorting: ")
print(myList)

for i in range(num):
    for j in range(i+1 , num):
        if (myList[i]>myList[j]):
            t = myList[j]
            myList[j] = myList[i]
            myList[i] = t
            
print("List after sorting: ")
print(myList)
            
