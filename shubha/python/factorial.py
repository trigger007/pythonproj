#Pyton program to find factorial of a number

def factorial(num):#function definition
    fact=1
#for loop for finding factorial
for i in range(1, num+1):
    fact=fact*i
#return factorial
    return fact  

number=int(input("enter any number to find factorial: "))

#function call and assign the value to variable result
result=factorial(number)
print("The factorial of %d = %d"%(number,result))