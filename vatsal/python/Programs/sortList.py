import random

NumList = []
Number = int(input("Enter number of list elements: "))
for i in range(1,Number+1):
    NumList.append(random.randint(1,101))
print("List Before Sorting: ")
print(NumList)
   
for i in range (Number):
    for j in range(i + 1, Number):
        if(NumList[i] > NumList[j]):
            temp = NumList[i]
            NumList[i] = NumList[j]
            NumList[j] = temp
print("List after Sorting: ")
print(NumList)
